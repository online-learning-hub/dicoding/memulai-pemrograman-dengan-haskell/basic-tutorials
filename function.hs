main = do
{-  Succ adalah fungsi yang mengembalikan successor dari sebuah bilangan
    Succ 8 mengembalikan 9 karena 9 adalah successor dari 8  -}
    print(succ 8)
    print(succ (9 * 10))
    -- Min adalah fungsi yang mengembalikan bilangan terkecil dari dua bilangan
    print(min 9 10)
    print(min 3.4 3.2)
    -- Max adalah fungsi yang mengembalikan bilangan terbesar dari dua bilangan
    print(max 100 101)
    print(succ 9 + max 5 4 + 1)
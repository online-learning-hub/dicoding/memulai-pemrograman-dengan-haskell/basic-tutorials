luasPersegiPanjang :: Int -> Int -> Int
{-
Int yang pertama adalah tipe data untuk parameter ke-1.
Int yang kedua adalah tipe data untuk parameter ke-2.
Int yang ketiga adalah tipe data untuk output.
-}

-- Kemudian, pada pendefinisian fungsi, kita hanya perlu memisahkan tiap parameter dengan spasi untuk menuliskan lebih dari satu parameter.
luasPersegiPanjang p l = p * l
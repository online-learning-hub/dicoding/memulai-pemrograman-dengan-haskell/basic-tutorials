-- Int memiliki batas maksimal 2147483647 dan batas minimal -2147483648
-- Integer tidak memiliki batas maksimal dan minimal, 
-- Meskipun biasanya digunakan untuk menyimpan angka dengan nilai besar, tipe Integer tetap bisa digunakan untuk nilai yang lebih kecil.
-- Float memiliki 7 digit angka di belakang koma
-- Double memiliki 15 digit angka di belakang koma
-- Bool hanya memiliki 2 nilai, yaitu True dan False
-- Char hanya bisa menampung 1 karakter
-- String adalah kumpulan dari Char


n :: Integer
n = 1234567890987654321987340982334987349872349874534

sangatBesar :: Integer
sangatBesar = 2^(2^(2^(2^2)))

main = do
    print(n)
    print(sangatBesar)
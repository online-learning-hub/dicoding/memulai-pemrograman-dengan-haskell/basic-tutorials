-- Dalam bahasa pemrograman Haskell, satu-satunya cara untuk melakukan perulangan adalah dengan rekursi (recursion).
fib n = do 
    if n == 0 then 1            
        else if n == 1 then 1
        else fib (n-1) + fib (n-2)

main = do
    let n = 2
    print(fib n)
    -- Jika ingin bilangan fibonacinya berderet, maka:
    print([fib x | x <- [0..n]])
    -- fib x adalah pemanggilan fungsi fib dengan parameter x
    -- x <- [0..n] adalah perulangan dari 0 sampai n

    -- Jika ingin bilangan fibonacinya berderet, tapi hanya yang ganjil, maka:
    print([fib x | x <- [0..n], odd (fib x)])
    -- Jika ingin bilangan fibonacinya berderet, tapi hanya yang genap, maka:
    -- print([fib x | x <- [0..n], even (fib x)])
    -- Jika ingin bilangan fibonacinya berderet, tapi hanya yang genap, dan hasilnya dikalikan 2, maka:
    -- print([fib x * 2 | x <- [0..n], even (fib x)])


    

-- Dalam bahasa pemrograman Haskell, tidak ada gagasan "x mesti dideklarasikan sebelum y" atau sebaliknya. Itu artinya, kita bisa menulis kode seperti ini.
-- Variabel harus memiliki nilai
y = x * 2
x = 3

main = do
    print y

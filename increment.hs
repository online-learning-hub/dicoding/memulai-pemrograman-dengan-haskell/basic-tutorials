-- Kita tidak bisa melakukan increment pada haskell
-- Sekali lagi, karena Haskell adalah pemrograman fungsional, pendekatannya juga sesuai dengan aturan matematis. 
-- Bayangkan, dengan Anda mengatakan n = n + 1 maka dalam konteks matematika, Anda menyebut bahwa 5 = 5 + 1. Jelas salah.

-- Jadi, bagaimana caranya agar kita bisa melakukan increment pada Haskell?
-- Jawabannya adalah dengan menggunakan fungsi.

inc n = n + 1

main = print(inc 5)
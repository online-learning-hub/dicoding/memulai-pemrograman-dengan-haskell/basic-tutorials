-- Membuat fungsi sendiri
f :: Int -> Int
-- f adalah nama fungsi
-- Int pertama adalah tipe data dari parameter yang diterima
-- Int kedua adalah tipe data dari nilai yang dikembalikan

-- f x = x*x
-- f adalah nama fungsi
-- x adalah parameter yang diterima
-- x*x adalah nilai yang dikembalikan
f x = x*x

main = do 
    print(f 5)
    print(f 10)
    print(f 15)
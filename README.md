## Tentang Haskell

Haskell adalah bahasa pemrograman yang berbasis fungsional dan lazim digunakan untuk pengembangan perangkat lunak yang kuat dan aman. Berikut adalah ringkasan singkat tentang Haskell:

**Pemrograman Fungsional:** Haskell adalah bahasa pemrograman fungsional, yang berarti fokus utamanya adalah pada fungsi dan komputasi yang diekspresikan melalui fungsi. Ini berbeda dengan paradigma pemrograman imperatif yang berfokus pada urutan instruksi.

**Referential Transparency:** Haskell menerapkan referential transparency, yang berarti bahwa ekspresi dalam Haskell selalu menghasilkan nilai yang sama jika diberikan input yang sama. Hal ini memungkinkan untuk mengganti ekspresi dengan hasilnya tanpa mengubah perilaku program.

**Imutabilitas:** Haskell menerapkan imutabilitas secara ketat, yang berarti setelah variabel diinisialisasi, nilainya tidak dapat diubah. Ini mendorong gaya pemrograman yang aman dan meminimalkan kesalahan yang berkaitan dengan perubahan variabel secara tidak sengaja.

**Statis dan Kuat Tipe Data:** Haskell adalah bahasa yang berjenis statis, yang berarti tipe data dari ekspresi ditentukan selama kompilasi, dan tipe-tipe tersebut tidak berubah selama eksekusi program. Haskell juga menggunakan tipe data yang kuat, yang memungkinkan deteksi banyak kesalahan tipe saat kompilasi.

**Evaluasi Malas (Lazy Evaluation):** Haskell menggunakan evaluasi malas, yang berarti ekspresi dievaluasi hanya ketika hasilnya diperlukan. Ini memungkinkan untuk bekerja dengan struktur data yang besar tanpa harus menghitung semua nilai secara sekaligus.

**Ekosistem Kaya:** Haskell memiliki ekosistem yang kaya dengan perpustakaan dan alat-alat yang kuat yang mendukung berbagai jenis pengembangan perangkat lunak, mulai dari pemrograman web hingga pemrosesan bahasa alami.

**Pengkodean yang Ekspresif:**
 Haskell sering dianggap memiliki kode yang ekspresif dan mudah dibaca. Gaya penulisan yang deklaratif memungkinkan programmer untuk menjelaskan apa yang ingin mereka capai tanpa perlu mengkhawatirkan detail implementasi.

## Catatan Mengenai Haskell:
- Jika kalian sudah terbiasa dengan bahasa pemrograman seperti Java, C++, PHP, atau bahkan JavaScript, tentu belajar bahasa Haskell ini akan terasa aneh dengan konsepnya. Secara umum, operator dan ekspresi dalam bahasa-bahasa ini memiliki kesamaan, namun ada beberapa perbedaan mendasar. Misalnya, dalam operator modulus, Haskell menggunakan mod, sedangkan bahasa lain biasanya menggunakan %. Meskipun ada perbedaan ini, konsep dasar operator dan ekspresi tetap konsisten antarbahasa.
- Kata kunci utama dari materi variabel adalah "immutable" dan "deklarasi tanpa urutan". Hal yang unik dalam bahasa pemrograman Haskell adalah kemampuan untuk mendeklarasikan variabel tanpa harus memperhatikan urutan. Ini berarti Anda dapat mendeklarasikan variabel apa pun di program Anda tanpa peduli pada urutan yang sesungguhnya digunakan. Selain itu, konsep "immutable" di Haskell mengacu pada ketidakmampuan untuk mengubah nilai setelah variabel diinisialisasi. Ini memberikan keamanan tambahan dan meminimalkan kesalahan dalam pengelolaan data.
- Sebagai seseorang yang tidak suka matematika, belajar tentang operator, ekspresi, dan variabel dalam pemrograman bahasa ini mungkin tampak rumit pada awalnya. Namun, bagi mereka yang sudah terbiasa dengan pemikiran matematika, konsep ini memiliki kesamaan dalam berbagai bahasa pemrograman. Meskipun perjalanan awalnya mungkin membingungkan, pemahaman ini akan menjadi landasan yang kuat untuk eksplorasi lebih lanjut dalam dunia pemrograman.

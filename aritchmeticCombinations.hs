{- 
Sebagai contoh, diberikan fungsi f(x) = 2 + 3x - x2 dan g(x) = 2x - 1. Cobalah untuk menggabungkan keduanya melalui penjumlahan fungsi dengan parameter bernilai 4. Ini bisa kita tulis sebagai (f + g)(4). Berikut adalah langkah-langkahnya:
(f + g)(x) = f(x) + g(x)
(f + g)(4) = f(4) + g(4)
(f + g)(4) = (2 + 3(4) - 42) + (2(4) - 1)
(f + g)(4) = -2 + 7
(f + g)(4) = 5
-}
f :: Int -> Int
f x = 2 + (3*x) - (x^2)
 
g :: Int -> Int
g x = (2*x) - 1

main = print(f 4 + g 4);

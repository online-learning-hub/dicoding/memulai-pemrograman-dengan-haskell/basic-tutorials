-- List dinotasikan dengan tanda kurung siku ([...]), sedangkan tuple dilambangkan dengan tanda kurung ((...)). 
-- Tuple dapat berisi komponen dengan kombinasi dari beberapa tipe data.
-- Tuple digunakan ketika Anda tahu persis berapa banyak nilai yang Anda inginkan.

main = do
    print((1,'a'))
    print((8,11),(4,5))
    print(("Christopher", "Walken", 55))
    print(((2,3), [2,3]))
    print([(1,2),(8,11),(4,5)])
    -- print([(1,2),(8,11,5),(4,5)]) -- Error, karena tuple tidak boleh berbeda jumlah komponennya
    -- Tuple hanya mengizinkan kita untuk memiliki tuple dalam list dengan ukuran komponen yang tetap,
    -- Baik hanya pair (2 Komponen), hanya triple (3 Komponen), dst.

    -- Tuple juga memiliki karakteristik urutan leksikografi seperti list.
    print((1,2) == (8,11))
    print((5,5,13) > (5,5,6))
    print((1,2) < (3,4))

    -- Operator pada tuple
    -- fst akan mengambil pair (tuple dengan dua komponen) dan mengembalikan komponen pertamanya.
    print(fst (8,11))
    -- snd akan mengambil pair (tuple dengan dua komponen) dan mengembalikan komponen keduanya.
    print(snd ("Wow", False))
    {- 
    Ingat selalu bahwa dua fungsi tersebut hanya dapat dioperasikan terhadap pair. 
    Mereka takkan bekerja untuk triple, 4-tuple, 5-tuple, dan seterusnya.
    Cara menggabungkan dua list menjadi tuple\
    -}
    print(zip [1,2,3,4,5] [5,5,5,5,5])
    print(zip [1..5] ["one", "two", "three", "four", "five"])
    -- Zip di bawah ini akan menghasilkan list yang lebih pendek, karena list kedua lebih pendek dari list pertama
    print(zip [5,3,2,6,2,7,2,5,4,6,6] ["im","a","turtle"])
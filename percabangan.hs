main = do   
    putStrLn "Enter a number:"
    str <- getLine
    let var = (read str :: Int) -- Konversi String ke Int
    -- let var = read str :: Int -- Konversi String ke Int
    
    -- Tak seperti pemrograman imperatif, sintaksis if di Haskell wajib memiliki bagian else. 
    -- Dalam pemrograman imperatif, kita bisa menulis bagian if saja tanpa else untuk mengabaikan langkah dengan kondisi yang tidak terpenuhi.
    if var == 0 
        then putStrLn "Number is zero" 
    else if var `mod` 2 == 0 
        then putStrLn "Number is Even" 
    else putStrLn "Number is Odd"

    -- Mengapa perlu else?
    -- Jawabannya adalah karena dalam Haskell, setiap ekspresi harus menghasilkan sebuah nilai.
    -- Jika Anda tidak menuliskan bagian else, maka ekspresi if akan menghasilkan nilai () (baca: unit).
    -- Unit adalah sebuah tipe data yang hanya memiliki satu nilai, yaitu ().
    -- Unit ini mirip dengan void di bahasa pemrograman Java atau C++.
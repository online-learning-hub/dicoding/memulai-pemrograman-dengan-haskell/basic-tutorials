-- deklarasi dan definisi fungsi luasSegitiga
luasSegitiga :: Double -> Double -> Double
luasSegitiga a t =  (a * t) / 2
-- deklarasi dan definisi fungsi volumePrismaSegitiga
volumePrismaSegitiga :: Double -> Double -> Double -> Double
volumePrismaSegitiga a t tp = luasSegitiga a t * tp
main = print(volumePrismaSegitiga 3 4 10)
-- List itu bersifat homogen. Tidak bisa campur tipe data seperti [1, "dua", 3, 4.0]] 
-- List bisa kosong, seperti []
-- List bisa berisi list lain, seperti [[1,2,3], [4,5,6], [7,8,9]]
-- List bisa berisi tuple, seperti [(1,2), (3,4), (5,6)]
bilanganPrima = [2, 3, 5, 7, 11, 13, 17, 19]
x = [[1,2,3,4],[5,3,3,3],[1,2,2,3,4],[1,2,3]] 

main = do
    -- Operator Konkatenasi
    print([1, 2, 3, 4] ++ [5, 6, 7, 8])
    print("Dicoding" ++ " " ++ "Indonesia")
    print(['H', 'a', 's'] ++ ['k', 'e', 'l', 'l'] )
    -- Operator Cons
    print('A':" SMALL CAT")
    print(1:[2,3,4,5])

    -- Cara mendapatkan nilai dari list
    print(bilanganPrima !! 1) -- Mengambil nilai dari list bilanganPrima pada indeks ke-1
    -- Cara mengambil nilai head atau pertama dari list
    print(head [5,4,3,2,1])
    -- Cara mengambil nilai tail atau semua nilai kecuali nilai head/pertama dari list
    print(tail [5,4,3,2,1])
    -- Cara mengambil nilai last atau nilai terakhir dari list
    print(last [5,4,3,2,1])
    -- Cara mengambil nilai init atau semua nilai kecuali nilai terakhir dari list
    print(init [5,4,3,2,1])
    -- Cara mengambil beberapa nilai dari list
    print(take 3 bilanganPrima) -- Mengambil 3 nilai pertama dari list bilanganPrima
    print(drop 3 bilanganPrima) -- Mengambil semua nilai dari list bilanganPrima kecuali 3 nilai pertama
    -- Cara mengambil nilai terbesar dan terkecil dari list
    print(maximum bilanganPrima)
    print(minimum bilanganPrima)
    -- Cara menghitung jumlah nilai pada list
    print(sum bilanganPrima)
    -- Cara menghitung perkalian nilai pada list
    print(product bilanganPrima)
    -- Cara mengecek apakah suatu nilai ada pada list
    print(elem 13 bilanganPrima)
    print(10 `elem` bilanganPrima)
    -- Cara mengecek apakah suatu list kosong
    print(null bilanganPrima)
    -- Cara menghasilkan list secara otomatis
    print([1..10])
    print([2,4..10])
    print([10,9..1])
    print([1,3..10])
    -- Cara reverse list dengan cara lain
    print(reverse [1..10])

    -- Komparasi pada list
    -- Untuk tiap komparasi, maka akan dibandingkan berdasarkan urutan ASCII
    -- Dan menggunakan urutan leksikografi (urutan kamus)
    print([3,2,1] > [2,10,100])
    -- Kenapa bernilai true? Karena 3 > 2
    -- Setelah itu, karena 3 > 2, maka nilai selanjutnya tidak diperiksa
    print("Educative" > "Education")
    -- Kenapa bernilai True? Karena semua charnya sama sampai index ke-7
    -- Setelah itu, karena v > o, maka nilai selanjutnya tidak diperiksa

    -- Yang perlu Anda ingat adalah karakteristik urutan leksikografi ini hanya berlaku pada operator <, <=, >, dan >= saja. 
    -- Sementara itu, operator == tidak demikian. Haskell akan membandingkan semua elemen.
    print([1,2,3] == [1,2,3,4])

    -- List di dalam List
    print(x)
    print(x !! 2)
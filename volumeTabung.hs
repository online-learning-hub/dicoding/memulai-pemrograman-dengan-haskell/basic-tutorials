luasLingkaran :: Double -> Double
-- pi adalah fungsi bawaan dari haskell
luasLingkaran r = pi*r*r
 
volumeTabung :: Double -> Double -> Double
volumeTabung r t = luasLingkaran r * t

main = print(volumeTabung 7 12)
-- Kenapa luasLingkarannya otomatis terisi tanpa kita mencari dulu luasLingkarannya?
-- Itu karena ketika kita mengisi r pada fungsi volumeTabung, maka fungsi luasLingkaran juga terisi dengan nilai r yang sama.
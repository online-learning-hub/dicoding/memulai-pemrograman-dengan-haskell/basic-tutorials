main = do
    print(3 + 2)
    print(19 - 27)
    print(2.35*8.6)
    print(8.7/3.1)
--  pembagian antar integer akan menghasilkan error
--  Namun karena kita tidak menuliskan tipe data, maka Haskell akan menganggapnya sebagai Float
--  Contoh 8/8 akan menghasilkan 1.0
    print(8/8)
-- Lantas bagaimana cara melakukan pembagian antar integer?
-- Kita bisa menggunakan fungsi div
    print (div 8 8)
    print (10 `div` 2)
    print(8/3)
    print(5^3)
    print(mod 19 3)
    print(19 `mod` 3)
--  print(-3*-7) ini akan error, karena -3 dan -7 bukan angka
--  print(-3*(-7)) ini akan benar, karena -3 dan -7 dianggap angka
    print(-3*(-7))

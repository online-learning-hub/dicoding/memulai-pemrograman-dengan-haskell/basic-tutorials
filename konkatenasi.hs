main = do
    print([1, 2, 3] ++ [4, 5])
    print("Dicoding" ++ " Indonesia")
    -- “Loh, bukannya operator konkatenasi hanya bisa menggabungkan list? Mengapa kali ini bisa menggabungkan dua buah string (kata)?” 
    -- Jawabannya adalah karena dalam Haskell, string (kata) sebenarnya adalah sebuah list of char.
    -- Dengan begitu, sebenarnya "Dicoding" sama dengan ['D', 'i', 'c', 'o', 'd', 'i', 'n', 'g']. Untuk membuktikannya, coba saja jalankan kode berikut, niscaya hasilnya akan True.
    print("Dicoding" == ['D', 'i', 'c', 'o', 'd', 'i', 'n', 'g'])

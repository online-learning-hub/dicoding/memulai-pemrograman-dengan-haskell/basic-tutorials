{-
Diketahui f(x) = x2 - x + 1 dan g(x) = 3x - 2. Coba kita hitung fungsi komposisinya.
(f o g)(x) = f(g(x))
Anda harus mengetahui arti dari notasi di atas terlebih dahulu. Ia memberi tahu bahwa kita mesti menghitung fungsi g(x) terlebih dahulu, kemudian memasukkan hasil nilainya ke dalam fungsi f(x).
Maka dari itu, kita bisa substitusi nilai g(x) dengan nilainya.
f(g(x)) = f(3x - 2)
Lanjut, kita substitusi nilai x dengan 5.
f(g(x)) = f(3(5) - 2)
Hasilnya seperti berikut.
f(g(5)) = f(13)
Setelah mendapatkan hasil dari fungsi g, nilai tersebut dijadikan sebagai input untuk fungsi f. Mari kita substitusi f(13) dengan nilainya.
f(13) = (13)2 - (13) + 1
Hasil akhir dari perhitungannya adalah sebagai berikut.
f(13) = 157
-}
f :: Int -> Int
f x = (x^2) - x + 1
 
g :: Int -> Int
g x = (3*x) - 2

main = print((f . g) 5 )